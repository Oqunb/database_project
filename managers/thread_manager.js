/**
 * Created by danil on 24.04.16.
 */
"use strict";
const Post = require('../models/post').Post;
const Thread = require('../models/thread').Thread;
const User = require('../models/user').User;
const db = require('../db');
const utils = require('../utils');
const sql_maker = require('../sql_makers/thread');

class ThreadManager {
    static list(success,error,options) {
        db.query(sql_maker.list(options)).then(function(result){
            success(result.map((thread) => Thread.view(thread)));
        }).catch(function(err){
            error(err);
        });
    }
    static list_posts(success,error,options) {
        db.query(sql_maker.post_list(options)).then(function(result){
            success(result.map((post) =>Post.view(post) ) );
        }).catch(function(err){
            error(err);
        });
    }
}
exports.ThreadManager = ThreadManager;