/**
 * Created by danil on 24.04.16.
 */
"use strict";
const Post = require('../models/post').Post;
const db = require('../db');
const sql_maker = require('../sql_makers/user');

class UserManager {
    static list_posts(success,error,options) {
        db.query(sql_maker.post_list(options)).then(function(result){
            success(result.map((post)=> Post.view(post)));
        }).catch(function(err){
            error(err);
        });
    }

}
exports.UserManager = UserManager;