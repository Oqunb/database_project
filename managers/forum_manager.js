/**
 * Created by danil on 24.04.16.
 */
"use strict";
const Post = require('../models/post').Post;
const Thread = require('../models/thread').Thread;
const User = require('../models/user').User;
const db = require('../db');
const utils = require('../utils');
const cache = require('../forum_cache');
const sql_maker = require('../sql_makers/forum');
const query_defaults = function (options) {
    return [options.forum,
        (options.since) ? options.since : '1000-01-01',
        (options.limit) ? options.limit : (1 << 30)];
};
const LIST_USERS_STMT = "Select DISTINCT uid as id from Posts  where forum = ? ";
class ForumManager {
    static list_users(success, error, options) {
        let sql = db.format(LIST_USERS_STMT, options.forum);
        sql += (options.since_id) ? ' and uid >=' + options.since_id : '';
        sql += ' order by uname ' + utils.get_sort_order(options);
        sql += (options.limit) ? ' limit  ' + options.limit : '';
        db.query(sql).then(function (result) {
            utils.array_fetch(result.map((user) => new User(user)), success, error, {key: 'id'});
        }).catch(function (err) {
            error(err);
        });
    }

    static list_threads(success, error, options) {
        let forum = (options.related & 1) ? cache.get(options.forum) : options.forum;
        options.related &= ~1; //исключить related forum
        let sql = sql_maker.thread_list(options);
        db.query(sql).then(function (result) {
            success(result.map(function (thread) {
                thread.forum = forum;
                if (options.related & 2) {
                    thread.user = User.view(thread, 'u');
                }
                return Thread.view(thread);
            }));
        }).catch(function (err) {
            error(err);
        });

    }

    static list_posts(success, error, options) {
        let forum = (options.related & 1) ? cache.get(options.forum) : options.forum;
        if (options.related >= 1) {
            let sql = "SELECT id from Posts where forum = ? and date >= ? order by date " + utils.get_sort_order(options) +
                " limit " + (options.limit || (1 << 30));
            db.query(sql, query_defaults(options)).then(
                function (result) {
                    utils.array_fetch(result.map(function (elem) {
                            return new Post(elem)
                        }),
                        success, error, options);
                }).catch(function (err) {
                error(err);
            });
        }
        else {
            options.related &= ~1;
            let sql = sql_maker.post_list(options);
            db.query(sql).then(function (result) {
                success(result.map(function (post) {
                    post.forum = forum;
                    if (options.related & utils.RELATED_MAP['user']) {
                        post.user = User.view(post, 'u');
                    }
                    if (options.related & utils.RELATED_MAP['thread']) {
                        post.thread = Thread.view(post, 't');
                    }
                    return Post.view(post);
                }));
            }).catch(function (err) {
                error(err);
            });
        }
    }
}
exports.ForumManager = ForumManager;