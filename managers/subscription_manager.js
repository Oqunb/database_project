/**
 * Created by danil on 08.04.16.
 */
"use strict";
const db =require('../db');
const TABLE_NAME = 'Subscriptions';
class SubscriptionManager{
    static subscribe(success,error,{user,thread}){
        let insert_options = {
            data: {
                'user':   user,
                'thread': thread
            },
            table: TABLE_NAME
        };
        db.insert_wrapper(insert_options, success, error);
    }
    static unsubscribe(success,error, {user,thread}){
        let sql = `CALL unsubscribe('${user}','${thread}')`;
        db.query_wrapper(sql, success, error);
    }
}
module.exports.SubscriptionManager =SubscriptionManager;