/**
 * Created by danil on 24.04.16.
 */
"use strict";
const Post = require('../models/post').Post;
const db = require('../db');
const utils = require('../utils');
const sql_maker = require('../sql_makers/post');

class PostManager {
    static list(success,error,options) {
        db.query(sql_maker.list(options)).then(function(result){
           success(result.map((post) => Post.view(post)));
        }).catch(function(err){
            error(err);
        });
    }
}
exports.PostManager = PostManager;