/**
 * Created by danil on 08.04.16.
 */
"use strict";
const db =require('../db');
const utils =require('../utils');
const User = require('../models/user').User;

const follow_list = function (type ,options, success, error){
    let sql = '';
    if(type == 'followers'){
        sql = 'Select wer_id as id from Followings where followee =? and wer_id >=? order by wer_id '
    }
    else{
        sql =  'Select wee_id as id from Followings where follower =? and wee_id >=? order by wee_id ';
    }
    sql += utils.get_sort_order(options);
    sql += ' limit ' + (options.limit || 1<<30);
    db.query(sql,[options.user, (options.since_id || 0)]).then(function(result){
        if(!result.length){
            success([]);
        }else {
            utils.array_fetch(result.map((user) => new User(user)),success,error,{key:'id'});
        }
    }).catch(function(err){
        error(err);
    });
};

class FollowingManager{
    static follow(success,error,{follower,followee}){
        db.query_wrapper(`CALL follow('${followee}','${follower}')`, success, error);
    }
    static unfollow( success,error,{follower,followee}){
        db.query_wrapper(`CALL unfollow('${followee}','${follower}')`, success, error);
    }
    static list_followers (success, error, options){
        follow_list('followers',options,success,error);
    }
    static list_following (success ,error, options){
        follow_list('followings',options,success,error);
    }
}
exports.FollowingManager = FollowingManager;