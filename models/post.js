/**
 * Created by danil on 07.04.16.
 */
"use strict";
const Thread = require('../models/thread').Thread;
const User = require('../models/user').User;
const utils = require('../utils');
const db = require('../db');
const dateformat =require('dateformat');
const cache = require('../forum_cache');

const IS_APPROVED = 1;
const IS_DELETED =2;
const IS_EDITED = 4;
const IS_HIGHLIGHTED =8;
const IS_SPAM=16;
const FULL_MASK =31;
const DATE_FORMAT ="yyyy-mm-dd HH:MM:ss";
const FETCH_PREPARED_STMT = "SELECT * FROM Posts WHERE id = ?";
const TABLE_NAME = 'Posts';
class Post{ //extends Model {
    constructor(sample) {
        //super();
        if (sample){
            this.id = sample.id;
            this._state = 0;
            this.isApproved = sample.isApproved;
            this.isDeleted = sample.isDeleted;
            this.isEdited = sample.isEdited;
            this.isHighlighted = sample.isHighlighted;
            this.isSpam = sample.isSpam;
            this.date = sample.date;
            this.likes = sample.likes;
            this.dislikes = sample.dislikes;
            this.message = sample.message;
            this.parent = sample.parent;
            this.thread = sample.thread;
            this.user = sample.user;
            this.forum = sample.forum;
        }
    }
    get isApproved(){
        return (this.stateMask & IS_APPROVED) !== 0;
    }
    get isDeleted(){
        return (this.stateMask & IS_DELETED) !== 0;
    }
    get isEdited(){
        return (this.stateMask & IS_EDITED) !== 0;
    }
    get isHighlighted(){
        return (this.stateMask & IS_HIGHLIGHTED) !== 0;
    }
    get isSpam(){
        return (this.stateMask & IS_SPAM) !== 0;
    }
    get points(){
        return (this.likes - this.dislikes);
    }
    set isApproved(new_value){
        if (new_value)
             this.stateMask |= IS_APPROVED;
        else
            this.stateMask &=FULL_MASK - IS_APPROVED;
    }
    set isDeleted(new_value){
        if (new_value)
            this.stateMask |= IS_DELETED;
        else
            this.stateMask &= FULL_MASK - IS_DELETED;
    }
    set isEdited(new_value){
        if (new_value)
            this.stateMask |= IS_EDITED;
        else
            this.stateMask &= FULL_MASK - IS_EDITED;
    }
    set isHighlighted(new_value){
        if (new_value)
            this.stateMask |= IS_HIGHLIGHTED;
        else
            this.stateMask &= FULL_MASK -  IS_HIGHLIGHTED;
    }
    set isSpam(new_value){
        if (new_value)
            this.stateMask |= IS_SPAM;
        else
            this.stateMask &=FULL_MASK -  IS_SPAM;
    }
    fetch(success,error,options){
        let sql = db.format(FETCH_PREPARED_STMT,[this.id]);
        db.query(sql,[],{'use_large_pool':true}).then(function (result) {
            if(result.length) {
                this.copy(result[0]);
                let related = [];
                if (options['related'] & utils.RELATED_MAP['forum']) {
                    this.forum = cache.get(this.forum);
                }
                if (options['related'] & utils.RELATED_MAP['user']) {
                    this.user = new User();
                    this.user.id = result[0].uid;
                    related.push(this.user);
                }
                if (options['related'] & utils.RELATED_MAP['thread']) {

                    this.thread = new Thread();
                    this.thread.id = result[0].thread;
                    related.push(this.thread);
                }
                if(related.length){
                    utils.array_fetch(related,success,error,{key:'id'});
                }
                else {
                    success(result);
                }
            }
            else{
                let e = new Error('Post with id ' + this.id + ' not found!');
                e.type ='not_found';
                error(e);
            }
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    save(success,error,options){

        var sql = 'SELECT insert_post(?,?,?,?,?,?,?) as id';
        sql = db.format(sql,[
              this.thread,
              this.user,
              this.forum,
              this.parent || 0,
              this.date,
              this.message,
              this.stateMask
        ]);
        db.query(sql).then(function(result){
            cache.inc('post');
            this.id = result[0].id;
            success(result);
        }.bind(this)).catch(function(err){
            error(err);
        });
        //db.insert(insert_options).then(function (result) {
        //    this.id = result.insertId;
        //    success(result);
        //}.bind(this)).catch(function (err) {
        //    error(err);
        //});
    }
    remove(success,error,options) {
        let update_options = {
            'set': 'stateMask = stateMask | (' + IS_DELETED+')',
            'where': 'id= ' +  this.id ,
            'table': TABLE_NAME
        };
        db.update(update_options).then(function (result) {
            success(result);
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    restore(success,error,options) {
        let update_options = {
            'set': 'stateMask = stateMask & (' + (FULL_MASK-IS_DELETED)+')',
            'where': 'id= ' +  this.id ,
            'table': TABLE_NAME
        };
        db.update(update_options).then(function (result) {
            success(result);
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    vote(success,error, options){
        let set_stmt = '';
        if (options.vote == 1){
            set_stmt = 'likes = likes +1';
        }
        else if (options.vote == -1){
            set_stmt = 'dislikes = dislikes +1';
        }
        else{
            error(new Error('Semantic error'));
        }
        let update_options = {
            'set': set_stmt,
            'where': 'id= ' + this.id,
            'table': TABLE_NAME
        };
        db.update(update_options).then(function (result) {
            this.fetch(success,error,{});
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    update(success,error,options){
        let update_options = {
            'set': 'message = ?',
            'where': 'id= ' +   this.id ,
            'table': TABLE_NAME
        };
        update_options['set'] = db.format(update_options['set'],[options.message]);
        db.update(update_options).then(function (result) {
            this.fetch(success,error,{});
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    toJSON(){
        return {
            id: this.id,
            isApproved: this.isApproved,
            isDeleted: this.isDeleted,
            isEdited: this.isEdited,
            isHighlighted: this.isHighlighted,
            isSpam: this.isSpam,
            points: this.points,
            parent: this.parent,
            date: this.date,
            message: this.message,
            likes: this.likes,
            dislikes: this.dislikes,
            thread: this.thread,
            forum: this.forum,
            user: this.user
        };
    }
    copy(sample){
        this.id = sample.id;
        this.stateMask = sample.stateMask;
        this.date = dateformat(sample.date,DATE_FORMAT);
        this.likes = sample.likes;
        this.dislikes = sample.dislikes;
        this.message = sample.message;
        this.parent = sample.parent || null;
        this.thread = sample.thread;
        this.user = sample.user;
        this.forum = sample.forum;
    }
    static view(db_post){
        return {
            id: db_post.id,
            isApproved:!! (db_post.stateMask & IS_APPROVED),
            isDeleted: !! (db_post.stateMask & IS_DELETED),
            isEdited: !! (db_post.stateMask & IS_EDITED),
            isHighlighted: !! (db_post.stateMask & IS_HIGHLIGHTED),
            isSpam: !! (db_post.stateMask & IS_SPAM),
            points: db_post.likes - db_post.dislikes,
            parent: db_post.parent || null,
            date: dateformat(db_post.date, DATE_FORMAT),
            message: db_post.message,
            likes: db_post.likes,
            dislikes: db_post.dislikes,
            thread: db_post.thread,
            forum: db_post.forum,
            user: db_post.user
        };
    }
}
Post.table = TABLE_NAME;
module.exports.Post = Post;
