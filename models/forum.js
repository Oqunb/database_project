/**
 * Created by danil on 07.04.16.
 */
"use strict";
const User = require('./user').User;
const db = require('../db');
const utils = require('../utils');
const TABLE_NAME = "Forums";
const cache = require('../forum_cache');
const FETCH_PREPARED_STMT = 'SELECT * FROM ' + TABLE_NAME + ' WHERE short_name = ?';
class Forum{
    constructor(sample) {
        if(sample) {
            this.id = sample.id;
            this.name = sample.name;
            this.short_name = sample.short_name;
            this.user = sample.user;
        }
    }
    toJSON() {
        return {
            id: this.id,
            name: this.name,
            short_name: this.short_name,
            user: this.user
        };
    }
    fetch(success, error, options) {
        let cache_result =cache.get(this.short_name);
        if(!cache_result){
            let e = new Error('forum with shortname' + this.short_name + ' not found!');
            e.type ='not_found';
            error(e);
        }
        else if (options['related'] && options['related'].has('user')) {
            let sql = db.format(FETCH_PREPARED_STMT,[this.short_name]);
            db.query(sql).then(function (result) {
                this.copy(result[0]);
                this.user = new User({email:result[0].user});
                this.user.fetch(success, error);
            }.bind(this)).catch(function (err) {
                error(err);
            });
        }
        else{
            this.copy(cache_result);
            success(cache_result);
        }
    }
       save(success, error, options) {
        let insert_options = {
            data: {
                'name': this.name,
                'short_name': this.short_name,
                'user': this.user
            },
            table: TABLE_NAME
        };
        db.insert(insert_options).then(function (result) {
            cache.inc('forum');
            this.id = result.insertId;
            cache.put(this);
            success(result);
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    copy(sample){
        this.id = sample.id;
        this.name = sample.name;
        this.short_name = sample.short_name;
        this.user = sample.user;
    }
}
Forum.table = TABLE_NAME;
module.exports.Forum= Forum;