/**
 * Created by danil on 07.04.16.
 */
"use strict";
const db = require('../db');
const cache = require('../forum_cache');
exports.table = 'Users';
class User  {
    constructor(sample){
        if(sample) {
            this.email = sample.email;
            this.id = sample.id;
            this.name = sample.name;
            this.username = sample.username;
            this.about = sample.about;
            this.isAnonymous = sample.isAnonymous;
        }
    }

    get following(){
        return (this._following) ? this._following.split(', '):[];
    }
    get followers(){
        return (this._followers) ? this._followers.split(', '):[];
    }
    get subscriptions(){
        return (this._subscriptions) ? this._subscriptions.split(', ').
        map(function(elem){
            return +elem;
        }) : [];
    }
    get isAnonymous(){
        return  !! this.is_anon;
    }
    set isAnonymous(new_value){
        this.is_anon =(new_value)?1:0;
    }
    set following(new_value){
        this._following= new_value;
    }
    set followers(new_value){
        this._followers= new_value;//(new_value !== '')? new_value.split(', '):[];
    }
    set subscriptions(new_value){
        this._subscriptions = new_value;// (new_value !== '')? new_value.split(', '):[];;
    }

    save(success, error, options) {
        let insert_options = {
            data: {
                'name': this.name,
                'email': this.email,
                'username': this.username,
                'isAnonymous': this.isAnonymous,
                'about': this.about
            },
            table: exports.table
        };
        db.insert(insert_options).then(function (result) {
            cache.inc('user');
            this.id = result.insertId;
            success(result);
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    fetch(success, error, options){
        let where = 'U.email = '+'\''+this.email+'\'';
        if(options && options.key == 'id'){
            where = 'id = ' + this.id;
        }
        var select_options ={
            columns :['U.id','U.email','U.about','U.name','U.username', 'U.isAnonymous' ,'U.followings', 'U.followers', 'U.subscriptions'],// 'F.followee', 'F.follower','S.thread'],
            from: 'Users as U', //left join Followings as F on (F.follower=U.email or F.followee = U.email) left join Subscriptions as S on S.user=U.email',
            where: where
        };
        db.select(select_options).then(function(result){
            if(result.length) {
                this.copy(result[0]);
                success(result);
            } else{
                let e = new Error('User with email ' + this.email + ' not found!');
                e.type ='not_found';
                error(e);
            }
        }.bind(this)).catch(function(err){
            error(err);
        })
    }
    update(success, error, options){
        let sql = 'CALL updateProfile(?,?,?)';
        let inserts = [this.email,this.name, this.about];
        //update_options['set'] = db.format(update_options['set'],[this.about, this.name]);
        db.query(sql,inserts).then(function (result) {
            this.fetch(success,error);
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    toJSON(){
        return {
            email : this.email,
            id : this.id,
            name : this.name,
            username : this.username,
            about : this.about,
            isAnonymous : this.isAnonymous,
            subscriptions : this.subscriptions,
            following: this.following,
            followers: this.followers
        }
    }
    copy(sample){
        this.email = sample.email;
        this.id = sample.id;
        this.name = sample.name;
        this.username = sample.username;
        this.about = sample.about;
        this.isAnonymous = sample.isAnonymous;
        this.following = sample.followings;
        this.followers = sample.followers;
        this.subscriptions = sample.subscriptions;
    }
    static view(db_user, prefix){
        let pref = prefix || '';
        return {
            email : db_user[pref + 'email'],
            id : db_user[pref +  'id'],
            name : db_user[pref +  'name'],
            username : db_user[pref +  'username'],
            about : db_user[pref +  'about'],
            isAnonymous : !! db_user[pref +  'isAnonymous'],
            subscriptions : db_user[pref +  'subscriptions']? db_user[pref +  'subscriptions'].split(', ').map((elem) => +elem) :[],
            following: db_user[pref +  'following']?db_user[pref +  'following'].split(', ') :[],
            followers: db_user[pref +  'followers']?db_user[pref +  'followers'].split(', '):[]
        }
    }
}
module.exports.User = User;