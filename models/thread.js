"use strict";
const User = require('./user').User;
const db = require('../db');
const utils= require('../utils');
const dateformat = require('dateformat');
const cache = require('../forum_cache');

const IS_DELETED = 1;
const IS_CLOSED = 2;
const DATE_FORMAT = 'yyyy-mm-dd HH:MM:ss';
const TABLE_NAME = "Threads";
const set_is_deleted = function(method,id ,success,error){
    db.query_wrapper(`CALL  ${method}_thread(${id})`, success, error);
};
const update_threads = function(set_stmt, success, error){
    let update_options = {
        'set': set_stmt,
        'where': 'id= ' +  this.id ,
        'table': TABLE_NAME
    };
    db.update_wrapper(update_options,success,error);
};
const FETCH_PREPARED_STATEMENT = 'SELECT * FROM ' + TABLE_NAME + ' WHERE id = ?';
class Thread {
    constructor(sample){
        if(sample){
            this.id = sample.id;
            this.isClosed = sample.isClosed;
            this.isDeleted = sample.isDeleted;
            this.message = sample.message;
            this.date = sample.date;
            this.likes = sample.likes;
            this.dislikes = sample.dislikes;
            this.slug = sample.slug;
            this.title = sample.title;
            this.user = sample.user;
            this.forum = sample.forum;
            this.posts = sample.posts;
        }
    }
    get isDeleted(){
        return (this.stateMask & IS_DELETED) != 0;
    }
    get isClosed(){
        return (this.stateMask & IS_CLOSED) != 0;
    }
    get points(){
        return (this.likes - this.dislikes);
    }
    set isClosed(new_value){
        if (new_value)
            this.stateMask |= IS_CLOSED;
        else
            this.stateMask &= ~IS_CLOSED;
    }
    set isDeleted(new_value){
        if (new_value)
            this.stateMask |= IS_DELETED;
        else
            this.stateMask &= ~IS_DELETED;
    }
    fetch(success,error,options){
        let sql = db.format(FETCH_PREPARED_STATEMENT,[this.id]);
        db.query(sql).then(function (result) {
            if(result.length) {
                this.id = result[0].id;
                this.stateMask = result[0].stateMask;
                this.message = result[0].message;
                this.date =dateformat( result[0].date, DATE_FORMAT);
                this.likes = result[0].likes;
                this.dislikes = result[0].dislikes;
                this.slug = result[0].slug;
                this.title = result[0].title;
                this.user = result[0].user;
                this.forum = result[0].forum;
                this.posts = result[0].posts;
                //this.copy(result[0]);
                let related = [];
                if (options['related'] & utils.RELATED_MAP['forum']) {
                    this.forum = cache.get(this.forum);
                }
                if (options['related'] & utils.RELATED_MAP['thread']) {
                    this.user = new User();
                    this.user.email = result[0].user;
                    this.user.fetch(success,error,{});
                }
                else {
                    success(result);
                }
            }
            else{
                let e = new Error('Thread with id' + this.id + ' not found!');
                e.type ='not_found';
                error(e);
            }
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    save(success,error,options){
        let insert_options = {
            data: {
                'stateMask': this.stateMask,
                'slug': this.slug,
                'user': this.user,
                'title': this.title,
                'forum': this.forum,
                'message':this.message,
                'date':this.date
            },
            table: TABLE_NAME
        };
        db.insert(insert_options).then(function (result) {
            cache.inc('thread');
            this.id = result.insertId;
            success(result);
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    update(success,error,options){
        let update_options = {
            'set': 'message = ?, slug = ?',
            'where': 'id= ' +   this.id ,
            'table': TABLE_NAME
        };
        update_options['set'] = db.format(update_options['set'],[options.message, options.slug]);
        db.update(update_options).then(function (result) {
            this.fetch(success,error,{});
        }.bind(this)).catch(function (err) {
            error(err);
        });
    }
    remove(success,error,options) {
        set_is_deleted('delete',this.id,success,error);
    }
    restore(success,error,options) {
        set_is_deleted('restore',this.id,success,error);
    }
    open(success,error,options) {
        let set_stmt = 'stateMask = stateMask & (' + (~IS_CLOSED)+')';
        update_threads.call(this,set_stmt,success,error);
    }
    close(success,error,options) {
        let set_stmt = 'stateMask = stateMask | (' + IS_CLOSED + ')';
        update_threads.call(this,set_stmt,success,error);
    }
    vote(success,error, options){
        let set_stmt = '';
        if (options.vote == 1){
            set_stmt = 'likes = likes +1';
        }
        else if (options.vote == -1){
            set_stmt = 'dislikes = dislikes +1';
        }
        update_threads.call(this,set_stmt,success,error);
    }
    toJSON(){
        return {
            id:this.id,
            isClosed:this.isClosed,
            isDeleted:this.isDeleted,
            message:this.message,
            date:this.date,
            likes:this.likes,
            dislikes:this.dislikes,
            points:this.points,
            slug:this.slug,
            title:this.title,
            user: this.user,
            forum: this.forum,
            posts: this.posts
        };
    }
    copy(sample){
        this.id = sample.id;
        this.stateMask = sample.stateMask;
        this.message = sample.message;
        this.date =dateformat( sample.date, DATE_FORMAT);
        this.likes = sample.likes;
        this.dislikes = sample.dislikes;
        this.slug = sample.slug;
        this.title = sample.title;
        this.user = sample.user;
        this.forum = sample.forum;
        this.posts = sample.posts;
    }
    static view(db_thread, prefix){
        let pref = prefix || '';
        return {
            id:db_thread[pref + 'id'],
            isClosed:!!(db_thread[pref  + 'stateMask'] & IS_CLOSED),
            isDeleted:!!(db_thread[pref  + 'stateMask'] & IS_DELETED),
            message:db_thread[pref  + 'message'],
            date:dateformat(db_thread[pref  +  'date'],DATE_FORMAT),
            likes:db_thread[pref  + 'likes'],
            dislikes:db_thread[pref  + 'dislikes'],
            points:db_thread[pref  + 'likes'] - db_thread[pref  + 'dislikes'],
            slug:db_thread[pref + 'slug'],
            title:db_thread[pref + 'title'],
            user: db_thread[pref + 'user'],
            forum: db_thread[pref + 'forum'],
            posts: db_thread[pref + 'posts']
        };
    }
}
Thread.pk = 'id';
Thread.table = TABLE_NAME;
module.exports.Thread = Thread;