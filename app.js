"use strict";
const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const router = require('./api/router');
const response = require('./response');
const cache = require('./forum_cache');
const db = require('./db');

cache.initialize();
let app = express();
let PORT =3000;
if (process.argv.length > 2) {
  PORT = process.argv[2];
}


//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

let request_counter = 0;

//app.use(function(req){
//  "use strict";
//  request_counter++;
//  console.log('Request count: ', request_counter);
//  req.next();
//});
//app.use(function(req){
//    if(req.body) {
//      console.log(`\nPost params: ${JSON.stringify(req.body)}`);
//    }
//    req.next();
//});
app.use('/db/api', router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not supports');
  err.status = 200;
  next(err);
});

app.use(function(err, req, res, next) {
  res.status(err.status || 200);
  res.json(response.err_response(err));
});

app.listen(PORT);
module.exports = app;
