const forum_maker = require('./forum');
const thread_maker = require('./thread');

exports.list = function({forum,thread, since='0000-01-01',limit=(1<<30),order='desc'}){
    if(forum){
        arguments[0].related = 0;
        return forum_maker.post_list(arguments[0])
    }
    else{
        arguments[0].sort = 'flat';
        return thread_maker.post_list(arguments[0]);
    }
};