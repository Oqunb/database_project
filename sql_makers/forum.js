const POST_LIST_MAP = {
    0:function(forum, since = '0000-00-00', order = 'desc', limit = (1 << 30)){//NOT RELATED
        return`
        SELECT id,date,likes,dislikes,stateMask,forum,message,parent,thread,user FROM Posts as P
        WHERE forum='${forum}' AND date >= '${since}'
        ORDER BY date ${order} LIMIT ${limit}
        `;
    },
    2:function(forum, since = '0000-00-00', order = 'desc', limit = (1 << 30)){//USER
        return `
        SELECT  STRAIGHT_JOIN P.id as id, P.date as date,P.likes as likes, P.dislikes as dislikes,P.stateMask AS stateMask,
        P.message as message, P.user as user,P.parent as parent,
        U.id as uid, U.email as uemail, U.isAnonymous as uisAnonymous, U.followers as ufollowers, U.followings as ufollowings,
        U.about as uabout, U.name as uname, U.username as uusername, U.subscriptions as usubscriptions
        FROM Posts as P INNER JOIN Users as U
        ON U.email = P.user
        WHERE forum='${forum}' AND date >= '${since}'
        ORDER BY date ${order} LIMIT ${limit}
        `;
    },
    4:function(forum, since = '0000-00-00', order = 'desc', limit = (1 << 30)){//Thread
        return `
        SELECT  STRAIGHT_JOIN P.id as id, P.date as date,P.likes as likes, P.dislikes as dislikes,P.stateMask AS stateMask,
        P.message as message, P.user as user, P.parent as parent,
        T.id as tid, T.date as tdate, T.likes as tlikes, T.dislikes as tdislikes, T.stateMask as tstateMask,T.forum as tforum,
        T.message as tmessage, T.slug as tslug, T.posts as tposts, T.user as tuser, T.title as ttitle
        FROM Posts as P INNER JOIN Threads as T
        ON T.id = P.thread
        WHERE P.forum='${forum}' AND P.date >= '${since}'
        ORDER BY P.date ${order} LIMIT ${limit}
        `;

    }
};
const THREAD_LIST_MAP ={
    0:function(forum, since = '0000-00-00', order = 'desc', limit = (1 << 30)){
        return`
        SELECT id,date,likes, dislikes,stateMask,forum, message,slug,posts,user, title FROM Threads
        WHERE forum='${forum}' AND date >= '${since}'
        ORDER BY date ${order} LIMIT ${limit}
        `;
    },
    2:function(forum, since = '0000-00-00', order = 'desc', limit = (1 << 30)){
        return `
        SELECT STRAIGHT_JOIN
        T.id as id, T.date as date, T.likes as likes, T.dislikes as dislikes, T.stateMask as stateMask,T.forum as forum,
        T.message as message, T.slug as slug, T.posts as posts, T.user as user, T.title as title,
        U.id as uid, U.email as uemail, U.isAnonymous as uisAnonymous, U.followers as ufollowers, U.followings as ufollowings,
        U.about as uabout, U.name as uname, U.username as uusername, U.subscriptions as usubscriptions
        FROM Threads as T INNER JOIN Users as U
        ON U.email = T.user
        WHERE forum='${forum}' AND date >= '${since}'
        ORDER BY date ${order} LIMIT ${limit}
        `;
    }
};
exports.post_list = function({related=0, forum, since, order, limit}){
     return POST_LIST_MAP[related](forum, since, order, limit);
};
exports.thread_list = function({related=0, forum, since, order, limit}){
    return THREAD_LIST_MAP[related](forum, since, order, limit);
};