/**
 * Created by danil on 24.06.16.
 */
"use strict";
const forum_maker = require('./forum');
const user_maker = require('./user');

let order_wrapper = function(order){
    "use strict";
    return utils.get_sort_order({'order':order});
};
const POST_LIST_MAP = {
    flat:function(thread, since='0000-00-00', order='desc', limit=(1<<30)){
        return`
        SELECT id,date,likes,dislikes,stateMask,forum,message,parent,thread,user FROM Posts as P
        WHERE thread='${thread}' AND P.date >= '${since}'
        ORDER BY P.date ${order} LIMIT ${limit}
        `;
    },
    tree:function(thread, since='0000-00-00', order='desc', limit=(1<<30)){
        let ord =order=='asc'?'P.path': 'P.root DESC , P.path ASC';
        return`
        SELECT id,date,likes,dislikes,stateMask,forum,message,parent,thread,user FROM Posts as P
        WHERE thread='${thread}' AND P.date >= '${since}'
        ORDER BY ${ord} LIMIT ${limit}
        `;
    },
    parent_tree:function(thread, since='0000-00-00', order='desc', limit=(1<<30)){
        let ord =order=='asc'?'P.path': 'P.root DESC , P.path ASC';
        return `
        SELECT id,date,likes,dislikes,stateMask,forum,message,parent,thread,user FROM Posts as P INNER JOIN
        (SELECT  root FROM Posts WHERE parent=0 AND thread= '${thread}' AND date >= '${since}'
         ORDER BY root ${order} LIMIT  ${limit}) as T
        ON P.root =T.root
        WHERE P.thread= '${thread}' AND P.date >= '${since}'
        ORDER BY ${ord};
        `;
    }
};
const THREAD_LIST_MAP ={

};
exports.post_list = function({sort='flat', thread, since, order, limit}){
    return POST_LIST_MAP[sort](thread, since, order, limit);
};
exports.list = function({forum,user, since='0000-01-01',limit=(1<<30),order='desc'}){
    if(forum){
        arguments[0].related = 0;
        return forum_maker.thread_list(arguments[0])
    }
    else{
        return user_maker.thread_list(arguments[0]);
    }
};
