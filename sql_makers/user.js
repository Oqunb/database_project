/**
 * Created by danil on 25.06.16.
 */

exports.post_list = function({user, since = '0000-00-00', order = 'desc', limit = (1 << 30)}){
    return `
    SELECT id,date,likes,dislikes,stateMask,forum,message,parent,thread,user FROM Posts as P
    WHERE user='${user}' AND date >= '${since}'
    ORDER BY date ${order} LIMIT ${limit}
    `;
};
exports.thread_list = function({user, since = '0000-00-00', order = 'desc', limit = (1 << 30)}){
    return `
    SELECT id,date,likes, dislikes,stateMask,forum, message,slug,posts,user, title FROM Threads
    WHERE user='${user}' AND date >= '${since}'
    ORDER BY date ${order} LIMIT ${limit}
    `;
};