module.exports = function (grunt) {

    grunt.initConfig({
        shell: {
            options: {
                stdout: true,
                stderr: true
            },
            server: {
                command: 'node app.js'
                //command: 'node server.js'
            }
        },
        watch: {
            server: {
                files: [
                    'api/**/*.js',
                    'managers/**/*.js',
                    'models/**/*.js',
                    'db.js',
                    'app.js'
                ]/*,
                tasks:['shell']/*,
                options: {
                    livereload: true
                }*/
            }
        },
        concurrent: {
            target: ['watch', 'shell'],
            options: {
                logConcurrentOutput: true
            }
        }
    });

	// подключть все необходимые модули
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-shell');

    grunt.registerTask('default', ['concurrent']);


};
