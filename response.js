/**
 * Created by danil on 23.03.16.
 */
"use strict";
exports.ok = function(data){
    return {'code':0,'response':data};
};
exports.err_response = function(err){
    let response_code = 4;
    if(err.type === "not_found"){
        response_code = 1;
    }
    else if(err.type === "semantic_error"){
        response_code = 3;
    }
    else if(err.type === "user_already_exists"){
        response_code=5;
    }
    else if(err instanceof SyntaxError){
        response_code =2 ;
    }
    return {code:response_code,response:err.message};
};