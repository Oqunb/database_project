/**
 * Created by danil on 21.03.16.
 */
"use strict";
const express = require('express');
const router = express.Router();
const db = require('../db');
const utils =  require('../utils');
const response = require('../response');
const Forum = require ('../models/forum').Forum;
const ForumManager = require('../managers/forum_manager').ForumManager;
const TABLE_NAME ='Forums';

router.use('/create',function(req,res){
    var forum = new Forum(req.body);
    forum.save(function(result){
        res.json(response.ok(forum));
    },function(err){
        if(err.errno === db.DB_ERRORS.DUPLICATE){
            forum.id = err.insertId;
            res.json(response.ok(forum));
        }else {
            req.next(err);
        }
    });
});
router.get('/details',function(req,res){
    let forum = new Forum({short_name:req.query.forum});
    req.query.related = utils.related_set(req.query.related);
    utils.api_wrapper(req,res, forum, 'fetch',forum);
});
router.use('/listPosts',function(req,res){
    utils.api_wrapper(req,res, ForumManager, 'list_posts');
});
router.use('/listThreads',function(req,res){
    utils.api_wrapper(req,res, ForumManager, 'list_threads');
});
router.use('/listUsers',function(req,res){
    utils.api_wrapper(req,res, ForumManager, 'list_users');
});
module.exports=router;