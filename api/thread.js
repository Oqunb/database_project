/**
 * Created by danil on 21.03.16.
 */
"use strict";
const express = require('express');
const router = express.Router();
const Thread = require('../models/thread').Thread;
const User = require('../models/user').User;
const SubscriptionManager=require('../managers/subscription_manager').SubscriptionManager;
const db = require('../db');
const utils = require('../utils');
const response = require('../response');
const ThreadManager = require('../managers/thread_manager').ThreadManager;


var update_thread = function(options,req,res){
    let thread = new Thread({id:req.body.thread});
    thread[options.method](function(result){
        if(options.extended_response)
            res.json(response.ok(thread));
        else
            res.json(response.ok(req.body));
    },function(err){
        req.next(err);
    }, req.body);
};

router.use('/create',function(req,res){
    let thread = new Thread(req.body);
    utils.api_wrapper(req,res, thread, 'save', thread);
});
router.use('/details',function(req,res){
    let thread = new Thread({id:req.query.thread});
    utils.api_wrapper(req,res, thread, 'fetch', thread);
});
router.use('/list',function(req,res){
    utils.api_wrapper(req,res, ThreadManager, 'list');
});
router.use('/listPosts',function(req,res){
    utils.api_wrapper(req,res, ThreadManager, 'list_posts');

});
router.use('/remove',function(req,res){
    update_thread({method:'remove'},req,res);
});
router.use('/restore',function(req,res){
    update_thread({method:'restore'},req,res);
});
router.use('/update',function(req,res){
    update_thread({method:'update',extended_response:true},req,res);
});
router.use('/vote',function(req,res){
    update_thread({method:'vote',extended_response:true},req,res);

});
router.use('/close',function(req,res){
    update_thread({method:'close'},req,res);
});
router.use('/open',function(req,res){
    update_thread({method:'open'},req,res);
});
router.use('/subscribe',function(req,res){
    utils.api_wrapper(req,res, SubscriptionManager, 'subscribe', req.body,req.body);
});
router.use('/unsubscribe',function(req,res){
    utils.api_wrapper(req,res, SubscriptionManager, 'unsubscribe',req.body,req.body);
});

module.exports=router;