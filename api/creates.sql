SET foreign_key_checks = 0;
DROP TABLE IF Exists Users;
CREATE TABLE Users (
  id int NOT NULL AUTO_INCREMENT,
  email VARCHAR(30) NOT NULL,
  about VARCHAR(5000), -- NOT NULL,
  isAnonymous tinyint NOT NULL,
  name VARCHAR(30) ,
  username VARCHAR(30),
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
)DEFAULT CHARSET=utf8;;

DROP TABLE IF Exists Followings;
CREATE TABLE Followings (
followee VARCHAR(30) NOT NULL,
follower VARCHAR (30) NOT NULL,
KEY followee (followee),
KEY follower (follower),
CONSTRAINT `Foll_ibfk_1` FOREIGN KEY (followee) REFERENCES Users (email) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `Foll_ibfk_2` FOREIGN KEY (follower) REFERENCES Users (email) ON DELETE CASCADE ON UPDATE CASCADE
)DEFAULT CHARSET=utf8;;

DROP TABLE IF Exists Forums;
CREATE TABLE Forums (
id int(10) NOT NULL AUTO_INCREMENT,
name VARCHAR(100) NOT NULL,
short_name VARCHAR(50) NOT NULL,
user VARCHAR(30) NOT NULL,
PRIMARY KEY (id),
UNIQUE KEY name(name),
UNIQUE KEY short_name(short_name),
KEY user (user),
CONSTRAINT `Forums_ibfk_1` FOREIGN KEY (user)   REFERENCES Users   (email) ON DELETE CASCADE ON UPDATE CASCADE
)DEFAULT CHARSET=utf8;;


DROP TABLE IF Exists Threads;
CREATE TABLE Threads (
id INT NOT NULL AUTO_INCREMENT,
forum VARCHAR(50) NOT NULL,
title VARCHAR(100) NOT NULL,
user VARCHAR(30) NOT NULL,
date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
message VARCHAR(10000) NOT NULL,
slug VARCHAR(100) NOT NULL,
stateMask TINYINT NOT NULL,
likes INT NOT NULL,
dislikes INT NOT NULL,
posts INT NOT NULL,
PRIMARY KEY(id),
KEY forum (forum),
KEY user  (user),
CONSTRAINT `Threads_ibfk_1` FOREIGN KEY (forum)  REFERENCES Forums  (short_name) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `Threads_ibfk_2` FOREIGN KEY (user)   REFERENCES Users   (email)      ON DELETE CASCADE ON UPDATE CASCADE
)DEFAULT CHARSET=utf8;;


DROP TABLE IF Exists Subscriptions;
CREATE TABLE Subscriptions (
user VARCHAR(30) NOT NULL,
thread INT NOT NULL,
KEY thread (thread),
KEY user  (user),
CONSTRAINT `Subs_ibfk_1` FOREIGN KEY (thread) REFERENCES Threads (id) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `Subs_ibfk_2` FOREIGN KEY (user)   REFERENCES Users   (email) ON DELETE CASCADE ON UPDATE CASCADE
)DEFAULT CHARSET=utf8;;

DROP TABLE IF Exists Posts;
CREATE TABLE Posts (
id INT NOT NULL AUTO_INCREMENT,
date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
message VARCHAR(150000) NOT NULL,-- not correct
parent INT NOT NULL DEFAULT 0,
stateMask TINYINT NOT NULL DEFAULT 0,
likes INT NOT NULL DEFAULT 0,
dislikes INT NOT NULL DEFAULT 0,
path VARCHAR(500) NOT NULL DEFAULT '',
root VARCHAR(7) NOT NULL DEFAULT '',
PRIMARY KEY(id),
thread INT NOT NULL,
user VARCHAR(30) NOT NULL,
forum VARCHAR(30) NOT NULL,
KEY forum (forum),
KEY thread (thread),
KEY user   (user),
CONSTRAINT Posts_ibfk_1 FOREIGN KEY (forum)  REFERENCES Forums  (short_name) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT Posts_ibfk_2 FOREIGN KEY (thread) REFERENCES Threads (id) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT Posts_ibfk_3 FOREIGN KEY (user)   REFERENCES Users   (email) ON DELETE CASCADE ON UPDATE CASCADE
) DEFAULT CHARSET=utf8;

ALTER TABLE Users ADD COLUMN subscriptions VARCHAR(255),
                  ADD COLUMN followings VARCHAR(255),
                  ADD COLUMN followers VARCHAR(255);
ALTER TABLE Followings ADD COLUMN wee_id INT NOT NULL, wer_id INT NOT NULL;
ALTER TABLE Posts ADD COLUMN uname VARCHAR(30) NOT NULL, ADD COLUMN uid INT NOT NULL;


DELIMITER |

DROP TRIGGER IF EXISTS update_posts_count_onupdate|
CREATE TRIGGER update_posts_count_onupdate AFTER UPDATE ON Posts FOR EACH ROW
BEGIN
    IF ((NEW.stateMask & 2) != 0  AND (OLD.stateMask & 2) = 0) THEN
        UPDATE Threads SET posts=posts - 1 WHERE id= NEW.thread;
    ELSEIF ((NEW.stateMask & 2) = 0  AND (OLD.stateMask & 2) != 0) THEN
        UPDATE Threads SET posts=posts + 1 WHERE id= NEW.thread;
    END IF;
END|
DELIMITER ;


DELIMITER //
DROP PROCEDURE IF EXISTS delete_thread//
CREATE PROCEDURE delete_thread(IN thread_id INT)
BEGIN
  START TRANSACTION;
  update Threads SET stateMask = (stateMask | 1) WHERE id=thread_id;
  update Posts SET stateMask = (stateMask | 2) WHERE thread=thread_id;
  COMMIT;
END //
DELIMITER ;

DELIMITER //
DROP PROCEDURE IF EXISTS restore_thread//
CREATE PROCEDURE restore_thread(IN thread_id INT)
BEGIN
  START TRANSACTION;
  update Threads SET stateMask = (stateMask & ~1) WHERE id=thread_id;
  update Posts SET stateMask = (stateMask  & ~2) WHERE thread=thread_id;
  COMMIT;
END //
DELIMITER ;


DELIMITER //
DROP Function IF EXISTS insert_post//
CREATE FUNCTION insert_post    (_thread INT,
								_user VARCHAR(30),
                                _forum VARCHAR(50),
                                _parent INT,
                                _date TIMESTAMP,
                                _message VARCHAR(15000),
								_stateMask TINYINT)
RETURNS INT
BEGIN

    DECLARE parent_path VARCHAR(500);
    DECLARE root_post VARCHAR(7);
    DECLARE new_path VARCHAR(500);
    DECLARE new_id INT(7) ZEROFILL;
    DECLARE _uid INT;
    DECLARE _uname VARCHAR(30);

    IF (_stateMask & 2) = 0 THEN
        UPDATE Threads SET posts=posts+1 WHERE id= _thread;
    END IF;

    INSERT INTO Posts  (thread,user,forum,parent,date,message,stateMask)
    VALUES (_thread,_user, _forum,_parent,_date, _message, _stateMask);
    SET new_id = LAST_INSERT_ID();
    IF (_parent != 0) THEN
        SELECT path INTO parent_path FROM Posts WHERE id = _parent;
        SELECT root INTO root_post FROM Posts WHERE id = _parent;
		SET new_path = CONCAT(parent_path,'.',CAST(new_id AS CHAR));
    ELSE
        SET new_path = CAST(new_id AS CHAR);
        SET root_post = new_path;
    END IF;

    SELECT id INTO _uid from Users WHERE email = _user;
    SELECT name INTO _uname from Users WHERE email = _user;

     IF ( _uname IS NOT NULL) THEN
        UPDATE Posts SET uid = _uid, uname =_uname, path = new_path, root = root_post WHERE id = new_id;
     ELSE
        UPDATE Posts SET uid = _uid, path = new_path, root = root_post WHERE id = new_id;
     END IF;
     -- UPDATE Posts SET path = new_path, root = root_post WHERE id = new_id;
    RETURN new_id;
END//
DELIMITER ;



DELIMITER //
DROP PROCEDURE IF EXISTS clear//
CREATE PROCEDURE clear()
BEGIN
     SET foreign_key_checks=0;
     TRUNCATE Posts;
     TRUNCATE Threads;
     TRUNCATE Forums;
     TRUNCATE Followings;
     TRUNCATE Subscriptions;
     SET foreign_key_checks=1;
END//
DELIMITER ;


DELIMITER //
DROP PROCEDURE IF EXISTS follow//
CREATE PROCEDURE follow(IN _followee VARCHAR(30),
                        IN _follower VARCHAR(30))
BEGIN
    DECLARE _wee_id INT;
    DECLARE _wer_id INT;
    SELECT id INTO _wee_id FROM Users where email=_followee;
    SELECT id INTO _wer_id FROM Users where email=_follower;
    INSERT INTO Followings (followee, follower,wee_id,wer_id)
    VALUES (_followee, _follower, _wee_id, _wer_id );
    UPDATE Users SET followers =  get_followers(_followee) where email = _followee;
    UPDATE Users SET followings = get_followings(_follower) where email = _follower;
END//
DELIMITER ;

DELIMITER //
DROP PROCEDURE IF EXISTS follow//
CREATE PROCEDURE follow(IN _followee VARCHAR(30),
                        IN _follower VARCHAR(30))
BEGIN
    DECLARE _wee_id INT;
    DECLARE _wer_id INT;
    SELECT id INTO _wee_id FROM Users where email=_followee;
    SELECT id INTO _wer_id FROM Users where email=_follower;
    INSERT INTO Followings (followee, follower,wee_id,wer_id)
    VALUES (_followee, _follower, _wee_id, _wer_id );
    UPDATE Users SET followers =  get_followers(_followee) where email = _followee;
    UPDATE Users SET followings = get_followings(_follower) where email = _follower;
END//
DELIMITER ;

DELIMITER //
DROP PROCEDURE IF EXISTS unfollow//
CREATE PROCEDURE unfollow(IN _followee VARCHAR(30),
                        IN _follower VARCHAR(30))
BEGIN

    DELETE FROM Followings where followee = _followee and follower = _follower;
    UPDATE Users SET followers =  get_followers(_followee) where email = _followee;
    UPDATE Users SET followings = get_followings(_follower) where email = _follower;
END//
DELIMITER ;

DELIMITER //
DROP PROCEDURE IF EXISTS unsubscribe//
CREATE PROCEDURE unsubscribe(IN _user VARCHAR(30),
                        IN _thread INT)
BEGIN
    DELETE FROM Subscriptions where thread = _thread and user = _user;
    UPDATE Users SET subscriptions =  get_subscriptions(_user) where email = _user;
END//
DELIMITER ;


DELIMITER //
    DROP FUNCTION IF EXISTS get_followers//
    CREATE FUNCTION get_followers(email VARCHAR(30))
    RETURNS VARCHAR (255)
    BEGIN
        DECLARE RES VARCHAR(255);
        SELECT GROUP_CONCAT(follower SEPARATOR ', ') INTO RES FROM Followings  WHERE followee = email;
        IF (RES IS NOT NULL) THEN
            RETURN RES;
        ELSE
            RETURN '';
        END IF;
    END//
DELIMITER ;
DELIMITER //
    DROP FUNCTION IF EXISTS get_followings//
    CREATE FUNCTION get_followings(email VARCHAR(30))
    RETURNS VARCHAR (255)
    BEGIN
        DECLARE RES VARCHAR(255);
        SELECT GROUP_CONCAT(followee SEPARATOR ', ') INTO RES FROM Followings  WHERE follower = email;
        IF (RES IS NOT NULL) THEN
            RETURN RES;
        ELSE
            RETURN '';
        END IF;
    END//
DELIMITER ;
DELIMITER //
    DROP FUNCTION IF EXISTS get_subscriptions//
    CREATE FUNCTION get_subscriptions(email VARCHAR(30))
    RETURNS VARCHAR (255)
    BEGIN
        DECLARE RES VARCHAR(255);
        SELECT GROUP_CONCAT(thread SEPARATOR ', ') INTO RES FROM Subscriptions  WHERE user = email;
        IF (RES IS NOT NULL) THEN
            RETURN RES;
        ELSE
            RETURN '';
        END IF;
    END//
DELIMITER ;
DELIMITER //

DROP PROCEDURE IF EXISTS updateProfile//
CREATE PROCEDURE updateProfile ( IN _user VARCHAR(30),
                                IN _name VARCHAR(30),
                                IN _about VARCHAR(5000)
)
BEGIN
    UPDATE Users SET name = _name, about= _about where email = _user;
    UPDATE Posts SET uname = _name WHERE user = _user;
END//
DELIMITER ;

DELIMITER //
DROP TRIGGER IF EXISTS on_subscribe//
CREATE TRIGGER on_subscribe AFTER INSERT ON Subscriptions FOR EACH ROW
BEGIN
    UPDATE Users SET subscriptions =  get_subscriptions(NEW.user) where email = NEW.user;
END//
DELIMITER ;





-- CREATE INDEX post_forum_user ON Posts(id,forum, user);
CREATE INDEX post_user ON Posts(user);
-- CREATE INDEX name_id_email ON Users(name,id,email);
-- CREATE INDEX user_forum ON Posts(user,forum);
CREATE INDEX user_date ON Posts(user,date);
CREATE INDEX forum_uname_uid ON Posts(forum,uname, uid);
CREATE INDEX date_id ON Threads(date, id);
CREATE INDEX thread_date ON Posts(thread ,date);
CREATE INDEX email_id ON Users(email, id);
CREATE INDEX wer ON Followings (follower, wee_id);
CREATE INDEX wee ON Followings (followee, wer_id);


SET foreign_key_checks = 1;

