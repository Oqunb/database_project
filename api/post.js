/**
 * Created by danil on 21.03.16.
 */
"use strict";
const express = require('express');
const router = express.Router();
const db = require('../db');
const response = require('../response');
const Post = require('../models/post').Post;
const PostManager = require('../managers/post_manager').PostManager;
const utils = require('../utils');

var update_post = function(options,req,res){
    let post = new Post({id:req.body.post});
    post[options.method](function(result){
        if(options.extended_response)
            res.json(response.ok(post));
        else
            res.json(response.ok(req.body));
    },function(err){
        req.next(err);
    }, req.body);
};

router.use('/create',function(req,res){
    let post = new Post(req.body);
    utils.api_wrapper(req,res, post, 'save', post, req.body);

});
router.use('/details',function(req,res){
    let post = new Post({id:req.query.post});
    utils.api_wrapper(req,res, post, 'fetch',post );

});
router.use('/list',function(req,res){
    utils.api_wrapper(req,res, PostManager, 'list');
});
router.use('/remove',function(req,res){
    update_post({method:'remove'},req,res);
});
router.use('/restore',function(req,res){
    update_post({method:'restore'},req,res);
});
router.use('/update',function(req,res){
    update_post({method:'update',extended_response:true},req,res);
});
router.use('/vote',function(req,res){
    update_post({method:'vote',extended_response:true},req,res);
});

module.exports=router;