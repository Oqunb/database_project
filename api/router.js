/**
 * Created by danil on 21.03.16.
 */
const express = require('express');
const express_router = express.Router();

const forum =  require('./forum');
const post =   require('./post');
const thread = require('./thread');
const user =   require('./user');
const common = require('./common');
const utils =  require('../utils');
const request_checker = require('./request_checker');

express_router.use(request_checker.check);
express_router.use(function(req){
    req.query.related = utils.related_set_binary(req.query.related);
    req.next();
});
express_router.use( '/forum', forum);
express_router.use( '/post', post);
express_router.use( '/thread', thread);
express_router.use( '/user', user);
express_router.use( '/', common);

module.exports = express_router;