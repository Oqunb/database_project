/**
 * Created by danil on 21.03.16.
 */
"use strict";
const express = require('express');
const router = express.Router();
const User = require('../models/user').User;
const UserManager = require('../managers/user_manager').UserManager;
const FollowingManager = require('../managers/following_manager').FollowingManager;
const db = require('../db');
const utils = require('../utils');
const response = require('../response');

var follow_handle = function (action, req, res) {
    let user = new User({email: req.body.follower});
    FollowingManager[action](function (result) {
        utils.api_wrapper(req, res, user, 'fetch', user);
    }, function (err) {
        req.next(err);
    }, req.body);
};

router.use('/create', function (req, res) {

    let user = new User(req.body);
    user.save(function (result) {
            res.json(response.ok(user));
        },
        function (err) {
            if (err.errno === db.DB_ERRORS.DUPLICATE) {
                err.message = 'User with email ' + user.email + ' is already exist';
                err.type = 'user_already_exists';
            }
            req.next(err);
        });
});

router.use('/details', function (req, res) {
    let user = new User({email: req.query.user});
    utils.api_wrapper(req, res, user, 'fetch', user);
});
router.use('/follow', function (req, res) {
    follow_handle('follow', req, res);
});
router.use('/listFollowers', function (req, res) {
    utils.api_wrapper(req, res, FollowingManager, 'list_followers');
});
router.use('/listFollowing', function (req, res) {
    utils.api_wrapper(req, res, FollowingManager, 'list_following');

});
router.use('/listPosts', function (req, res) {
    utils.api_wrapper(req, res, UserManager, 'list_posts');
});
router.use('/unfollow', function (req, res) {
    follow_handle('unfollow', req, res);
});
router.use('/updateProfile', function (req, res) {
    let user = new User(req.body);
    user.email = req.body.user;
    utils.api_wrapper(req, res, user, 'update', user);
});

module.exports = router;