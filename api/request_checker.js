/**
 * Created by danil on 25.06.16.
 */
"use strict";

const FORUM_LIST_ALLOWABLE = {'forum':1,'limit':1,'related':1,'order':1,'since':1};
const FORUM_LIST_USERS_ALLOWABLE = {'forum':1,'limit':1,'related':1,'order':1,'since_id':1};
const POST_POSIBLE_RELATED = {'forum':1,'user':1,'thread':1};
const THREAD_POSIBLE_RELATED ={'forum':1,'user':1};
const FORUM_POSIBLE_RELATED = {'user':1};
const FOLLOW_LIST_OPTONS ={
    required: ['user'],
    allowable: {'user':1,'since_id':1,'order':1,'limit':1}
};

const CHECK_OPTIONS = {
    '/forum/details/':{
        required: ['forum'],
        allowable: {'forum':1,'related':1},
        related: FORUM_POSIBLE_RELATED
    },
    '/forum/listPosts/':{
        required: ['forum'],
        allowable: FORUM_LIST_ALLOWABLE,
        related: POST_POSIBLE_RELATED
    },
    '/forum/listThreads/':{
        required: ['forum'],
        allowable: FORUM_LIST_ALLOWABLE,
        related: THREAD_POSIBLE_RELATED
    },
    '/forum/listUsers/':{
        required: ['forum'],
        allowable: FORUM_LIST_USERS_ALLOWABLE,
        related: {}
    },
    '/post/details/':{
        required:['post'],
        allowable: {'post':1,'related':1},
        related: POST_POSIBLE_RELATED
    },
    '/post/list/':{
        allowable: {'forum':1,'thread':1,'since':1,'limit':1,'order':1}
    },
    '/user/details/':{
        required: ['user']
    },
    '/user/listFollowers/':FOLLOW_LIST_OPTONS,
    '/user/listFollowing/': FOLLOW_LIST_OPTONS,
    '/user/listPosts/':{
        required:['user'],
        allowable: {'user':1,'since':1,'limit':1,'order':1}
    },
    '/thread/details/':{
        required:['thread'],
        allowable: {'thread':1,'related':1},
        related: THREAD_POSIBLE_RELATED
    },
    '/thread/listPosts/':{
        required:['thread'],
        allowable: {'thread':1,'sort':1, 'since':1,'limit':1,'order':1}
    },
    '/thread/list/':{
        allowable: {'user':1,'forum':1,'since':1,'limit':1, 'order':1}
    }

};

const check_required = function(params, path){
        let tmp = CHECK_OPTIONS[path].required;
        return !tmp || tmp.every((elem) => (elem in params));
};
const check_allowable = function(params, path){
    let tmp = CHECK_OPTIONS[path].allowable;
    return !tmp || Object.keys(params).every((elem) => elem in tmp);
};
const check_related = function(params, path){
    let tmp = CHECK_OPTIONS[path].related;
    if( !tmp || !params.related){
        return true;
    }
    else{
        let related =  params.related;
        if(related instanceof  Array){
            return related.every((elem) =>elem in tmp);
        }
        else{
            return (related in tmp);
        }
    }
};
const check_all = function(params,path){
    return check_required(params,path) && check_allowable(params,path) && check_related(params,path);
};
exports.check = function(req,res){
    if(CHECK_OPTIONS[req.path]) {
        if (check_all(req.query, req.path)) {
            req.next();
        }
        else {
            let err = new Error();
            err.type = 'semantic_error';
            err.message = 'Invalid params in request';
            req.next(err)
        }
    }
    else{
        req.next();
    }
};