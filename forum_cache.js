/**
 * Created by danil on 21.06.16.
 */
"use strict";
const db = require ('./db');

let status_cache ={
    'user':0,
    'thread':0,
    'post':0,
    'forum':0
};
let forum_cache = {};
const initialize_status = function(){
    db.query(
        `SELECT COUNT(*) as ans from Users UNION ALL
         SELECT COUNT(*) as ans from Threads UNION ALL
         SELECT COUNT(*) as ans from Posts UNION ALL
         SELECT COUNT(*) as ans from Forums`
    ).then(function(result){
        status_cache.user = result[0].ans;
        status_cache.thread = result[1].ans;
        status_cache.post = result[2].ans;
        status_cache.forum = result[3].ans;
    }).catch(function (err) {
        throw err;
    });
};

exports.put = function(forum){
    forum_cache[forum.short_name] = forum;
};
exports.get = function(short_name){
    return forum_cache[short_name];
};
exports.initialize = function(){
    db.query("Select * from Forums").then(
        function(result){
            for(let i=0; i< result.length; i++) {
                exports.put(result[i]);
            }
            initialize_status();
        }
    ).catch(function(err){
        if(err.message == 'pool.query is not a function'){
            exports.initialize();
        }
        else {
            throw err;
        }
    })
};
exports.clear = function(){
    forum_cache = {};
    status_cache ={
        'user':0,
        'thread':0,
        'post':0,
        'forum':0
    };
};
exports.status = function(){
    return status_cache;
};
exports.inc = function(type){
    status_cache[type]++;
};