/**
 * Created by danil on 21.03.16.
 */
"use strict";
const mysql = require('mysql');
let pool =mysql.createPool({
    host: 'localhost',
    user: 'newuser',
    password: 'password',
    database: 'maindb',
    connectionLimit: 300
});

const KEYS ={
  'Users':'email',
   'Forums':'short_name'
};
exports.query = function (sql, props, options) {
    return new Promise(function (resolve, reject) {
        pool.query(sql,props,function (err, res) {
            if (err){
                if(err.errno === exports.DB_ERRORS.DUPLICATE) {
                    let key = KEYS[options['table']];
                    let sql = 'SELECT id from ' + options['table'] + ' where ' + key + ' = ' + '\'' + options['data'][key] + '\'';
                    pool.query(sql, {},
                        function (error, result) {
                            if (error) {
                                reject(error);
                            }
                            else {
                                err.insertId = result[0].id;
                                reject(err);
                            }
                        }
                    );
                }
                else {
                    reject(err);
                }
            }
            else {
                resolve(res);
            }
        });
    });
};

exports.format= function(sql,inserts){
    return mysql.format(sql,inserts);
};
exports.DB_ERRORS ={
    'DUPLICATE':1062
};
exports.insert = function(options){
    let columns = Object.keys(options['data']).length;
    let aux = function(columns){
        return  '('+new Array(columns).join(' ?,') + ' ?)';
    };

    let sql = 'INSERT INTO ' + options['table'] +' ( ' + Object.keys(options['data']).join(', ')  +')  values '+aux(columns);
    let inserts = [];
    for (let key in options['data']){
        inserts.push(options['data'][key]);
    }

    return exports.query( mysql.format(sql, inserts),{},options );
};
exports.delete =function(options){
     let where =' where '+options['where'];
     let limit = (options['limit']) ? (' limit' + options['limit']) : '';
     let sql = 'delete from '+ options['table']+ ' ' + where + limit;
     return exports.query(sql);
};
exports.select = function(options){
    let columns = ((typeof options['columns']) ===  'string') ? options['columns'] : options['columns'].join(', ');
    let where  = (options['where']?(' where ' + options['where']):'');
    let orderby = options['order_by']?( (typeof(options['order_by']) === 'string') ? options['order_by'] : options['order_by'].join(', ')):'';
    orderby = orderby?' order by ' + orderby:'';
    let limit = ( options['limit'] ? ' limit '  + options['limit']:'');
    let sql = 'SELECT DISTINCT '+ columns + ' FROM ' +  options['from'] + where + orderby +limit;
    return exports.query(sql,{},options);
};
exports.update = function(options){
    let where =' where '+options['where'];
    let set_c = ' set ' + options['set'];
    let sql ='update '+options['table'] + set_c + where;
    return exports.query(sql);
};
const generalized_wrapper  = function(type,query,success, error){
    exports[type](query).then(function(result){
        success(result);
    }).catch(function(err){
        error(err);
    })
};
exports.query_wrapper = function(sql, success, error){
    generalized_wrapper('query',sql,success,error);
};
exports.update_wrapper = function(options, success, error){
    generalized_wrapper('update', options,success,error);
};
exports.insert_wrapper = function(options, success, error){
    generalized_wrapper('insert', options,success,error);
};
exports.delete_wrapper = function(options, success, error){
    generalized_wrapper('delete', options,success,error);
};
exports.select_wrapper = function(options, success, error){
    generalized_wrapper('select', options,success,error);
};

exports.tables =['Users','Posts','Forums','Threads',
                'Subscriptions', 'Followings'];
